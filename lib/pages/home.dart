import 'package:flutter/material.dart';
import 'package:flutter_dashboard_3/widgets/bar_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  double sliderValue = 15;
  charts.Color testColor = charts.Color(r: 64, g: 196, b: 255);


  @override
  Widget build(BuildContext context) {
    charts.Color determineBarColor(int value) {
      charts.Color resultColor;
      if (value >= 0 && value <= 50) {
        resultColor = charts.Color(r: 64, g: 196, b: 255);
      } else if (value > 50 && value <= 95) {
        resultColor = charts.Color(r: 76, g: 175, b: 80);
      } else if (value > 95 && value <= 110) {
        resultColor = charts.Color(r: 255, g: 152, b: 0);
      } else {
        resultColor = charts.Color(r: 244, g: 67, b: 54);
      }
      return resultColor;
    }

    List<charts.Series<OrdinalSales, String>> tempDisplay = [
      charts.Series<OrdinalSales, String>(
          id: 'Sales',
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          colorFn: (OrdinalSales segment, _) => segment.color,
          data: [
            OrdinalSales('1', 50, determineBarColor(50)),
            OrdinalSales('2', 25, determineBarColor(25)),
            OrdinalSales('3', sliderValue.toInt(),
                determineBarColor(sliderValue.toInt())),
            OrdinalSales('4', 75, determineBarColor(75)),
          ],
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
              '${sales.sales.toString()} °C')
    ];

    return Scaffold(
      drawer: Drawer(),
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Home'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(50.0),
            height: 300.0,
            child: BarChart(
              tempDisplay,
              animate: false,
            ),
          ),
          Container(
            child: Slider(
              activeColor: Colors.amber,
              inactiveColor: Colors.amber.withOpacity(.2),
              min: 0,
              max: 120,
              divisions: 120,
              onChanged: (value) {
                setState(() {
                  this.sliderValue = value;
                });
              },
              value: sliderValue,
            ),
          )
        ],
      ),
    );
  }
}
